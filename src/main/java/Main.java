import java.util.logging.Level;
import java.util.logging.Logger;

public class Main implements ExamplePresenter.View{


    public static void main(String[] args) {
        ExamplePresenter.View d = new Main();
        ExamplePresenter presenter = new ExamplePresenter(d);
	dd(presenter);


    }

    private static void dd(ExamplePresenter presenter) {
        presenter.compareAB("A","A");
    }

    private final static Logger LOGGER = Logger.getLogger(Main.class.getName());
    public void showNotCompare() {
        LOGGER.setLevel(Level.INFO);
        LOGGER.warning("Not compare");
    }

    public void showIsCompareEiEi(int countEiEi) {
        LOGGER.setLevel(Level.INFO);
        LOGGER.warning("is compare : "+ countEiEi);
    }
}
