import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class ExamplePresenterTest {

    @Mock
    private
    ExamplePresenter.View view;

    @Test
    public void compareStringSuccessItShouldShowIsCompareEiEi() throws Exception {
        MockitoAnnotations.initMocks(this);
        ExamplePresenter presenter = new ExamplePresenter(view);
        ExamplePresenter spyPresenter = spy(presenter);

        String strA = "A";
        String strB = "A";

        spyPresenter.compareAB(strA, strB);

        verify(spyPresenter).checkLogicSomething(anyString());
        verify(spyPresenter).countEiEiEi(anyString());
        verify(view).showIsCompareEiEi(anyInt());
    }

    @Test
    public void compareStringSuccessItShouldShowNotCompareEiEi() throws Exception {
        MockitoAnnotations.initMocks(this);
        ExamplePresenter presenter = new ExamplePresenter(view);
        ExamplePresenter spyPresenter = spy(presenter);

        String strA = "A";
        String strB = "B";

        spyPresenter.compareAB(strA, strB);

        verify(spyPresenter).countEiEiEi(anyString());
        verify(view).showNotCompare();
    }

}